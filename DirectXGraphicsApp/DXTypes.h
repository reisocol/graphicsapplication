#pragma once

#include <memory>
//-----------------------------------------------------------------------------
// Base Typedefs
//-----------------------------------------------------------------------------

typedef unsigned char           dxByte;
typedef char                    dxInt8;
typedef unsigned char           dxgUInt8;
typedef short                   dxInt16;
typedef unsigned short          dxgUInt16;
typedef long                    dxInt32;
typedef unsigned long           dxUInt32;
typedef signed __int64          dxInt64;
typedef unsigned __int64        dxUInt64;
typedef int                     dxInt;
typedef unsigned int            dxUInt;
typedef float                   dxFloat;
typedef double                  dxDouble;
typedef float                   dxReal;

#define DX_NULL 0