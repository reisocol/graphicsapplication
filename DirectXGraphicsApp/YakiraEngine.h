/*******************************************************
                        __   .__
         ___.__._____  |  | _|__|___________
        <   |  |\__  \ |  |/ /  \_  __ \__  \
         \___  | / __ \|    <|  ||  | \// __ \_
         / ____|(____  /__|_ \__||__|  (____  /
         \/          \/     \/              \/

-----------------------------------------------------        
               Yakira Game Engine v0.2
        Copyright (c) 2020-2025 - Rodrigo Reis
-----------------------------------------------------

********************************************************/

#pragma once

#ifndef _YAKIRA_ENGINE_H
#define _YAKIRA_ENGINE_H

#include "DXAPI.h"
#include "DXPlaftormDefines.h"
#include "DXTypes.h"
#include "DXConfig.h"

#endif _YAKIRA_ENGINE_H